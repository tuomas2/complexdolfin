"""DEMO
   Complex-valued Helmholtz equation in a unit square, 
   with absorbing boundary conditions.
   2013 Tuomas Airaksinen
"""
# SYMPY PART
from sympy import *
from complexdolfin.sympy_util import *

# Define complex-valued and real-valued symbols
u,v,f = symbols("u,v,f")
rho,k = symbols("rho,k",real=True)

# Helmholts equation with absorbing boundary condition, as a SymPy expression
a = (1/rho*inner(grad(u),grad(v))*dx 
     - k**2*u*v*dx 
     - f*v*dx 
     - 1j*k*u*v*ds
     )

k_sym = k 
# DOLFIN PART
from dolfin import *
from complexdolfin.dolfin_util import *

mesh = UnitSquareMesh(30,30)
V = ComplexFunctionSpace(mesh,"CG",1)

# Define mapping between SymPy objects and DOLFIN objects
S2U = SymPy2UFL({u :     TrialFunctions(V),
                 v :     TestFunctions(V),
                 k_sym : Constant(15.0),
                 rho :   Constant(1.2),
                 f :    Constant(0.0j)        })

# Convert SymPy expression to UFL expression
a = S2U(a)

A = assemble(lhs(a))
b = assemble(rhs(a))

# Place source term in the middle
try:
    from finddof import finddof
    centerdof = finddof((0.5,0.5),V)[0] 
except: #finddof fails with dolfin <= 1.2.0 
    centerdof = int(len(b)/2+1)

b[centerdof] = 1.0

u = Function(V)
solve(A,u.vector(),b)
re_u, im_u = u.split()

plot(re_u,interactive=True)
