#encoding:utf-8
from scipy.special import jnp_zeros, jn 
from numpy import *
dj0 = array([  0.            ,   1.841183781341,   3.054236928227,
               3.831705970208,   4.201188941211,   5.317553126084,
               5.331442773525,   6.4156163757  ,   6.706133194158,
               7.015586669816,   7.501266144684,   8.015236598376,
               8.536316366346,   8.577836489714,   9.282396285242,
               9.647421651997,   9.969467823088,  10.173468135063,
              10.519860873772,  10.7114339707  ,  11.345924310743,
              11.706004902592,  11.734935953043,  11.770876674956,
              12.681908442639,  12.826491228033,  12.93238623709 ,
              13.170370856016,  13.323691936314,  13.878843069697])

#this root's sequence number in between the roots of bessel function of the same order
nn = [ 0,  1,  2,  0,  3,  4,  1,  5,  2,  0,  6,  3,  1,  7,  4,  8,  2,
      0,  5,  9,  3,  1,  6, 10,  4, 11,  7,  2,  0, 12]

#the order of related bessel function
m =   [0, 1, 1, 1, 1, 1, 2, 1, 2, 2, 1, 2, 3, 1, 2, 1, 3, 3, 2, 1, 3, 4, 2,
       1, 3, 1, 2, 4, 4, 1]

#cc: Integral I_n values after multiplying with r²
cc = [ 6.283185,  1.534873,  1.155113,  6.283185,  0.34926 ,  1.147676,
       6.232203,  0.95417 ,  3.689065,  6.283185]

def calc(k, r1, r2, nmodI, nmodO):
    global nrealI,nrealO
    #bessel derivative roots, we need list of them
    rr = r1**2 / r2**2

    #roots of bessel_n_m
    nt = len(dj0)

    mxn = max(nn)

    if dj0[-1] < k*r1 or dj0[-1] < k*r2:
        raise Exception("More roots needed")

    lambdaI = zeros(nt,dtype=complex)
    lambdaO = zeros(nt,dtype=complex)
    kappaI = zeros(nt,dtype=float)
    kappaO = zeros(nt,dtype=float)
    sclI = zeros(nt,dtype=float)
    sclO = zeros(nt,dtype=float)

    nrealI = 0
    for i in range(nt):
        #radial wavenumber kr, previously kp1,kp2, kappa1
        scl = sclI
        lamda = lambdaI
        kappa = kappaI

        kr = dj0[i] / r1 
        #axial wavenumber kz
        assert complex(k**2 - kr**2).imag == 0.
        kz = sqrt(k**2 - kr**2+0.j)

        #non-convective:
        lamda[i] = kz

        #convective:
        #M = vz[2]/c
        #lambdaI[i] = (k*M-sqrt(complex(kr*2*M**2+kz**2)))/(1.-M**2)

        kappa[i] = kr

        if kr < k: nrealI += 1

        bj = [ jn(j,dj0[i]) for j in range(mxn+1) ]

        if nn[i]==0:
            scl[i] = 1.0/ sqrt(pi*r1**2*(bj[1]**2 + bj[0]**2))
        else:
            ik = nn[i]
            scl[i] = 1.0/ sqrt(pi*r1/kr * (dj0[i]*bj[ik-1]**2 
                      - 2.0*ik*bj[ik]*bj[ik-1] + dj0[i]*bj[ik]**2))
    nrealO = 0
    for i in range(nt):
        scl = sclO
        lamda = lambdaO
        kappa = kappaO

        kr = dj0[i] / r1 
        #axial wavenumber kz
        assert complex(k**2 - kr**2).imag == 0.
        kz = sqrt(k**2 - kr**2+0.j)

        #non-convective:
        lamda[i] = kz

        #convective:
        #M = vz[2]/c
        #lambdaI[i] = (k*M-sqrt(complex(kr*2*M**2+kz**2)))/(1.-M**2)

        kappa[i] = kr

        if kr < k: nrealO += 1

        bj = [ jn(j,dj0[i]) for j in range(mxn+1) ]

        if nn[i]==0:
            scl[i] = 1.0/ sqrt(pi*r1**2*(bj[1]**2 + bj[0]**2))
        else:
            ik = nn[i]
            scl[i] = 1.0/ sqrt(pi*r1/kr * (dj0[i]*bj[ik-1]**2 
                      - 2.0*ik*bj[ik]*bj[ik-1] + dj0[i]*bj[ik]**2))

    sumlambdaI = sum(lambdaI[:nrealI])

    fc = ([sqrt(sclI[0]**2*cc[0]/(sumlambdaI*sclI[i]**2*cc[i])) for i in range(nrealI)] 
          + list(zeros(nmodI-nrealI,dtype=complex)))
    print "Number of non-imaginary modes (inlet, outlet):", nrealI, nrealO
    return lambdaI,lambdaO,kappaI,kappaO,sclI,sclO,fc

