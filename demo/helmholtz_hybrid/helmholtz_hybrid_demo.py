#encoding:utf-8
"""(A more advanced) complexdolfin DEMO

   Hybrid method for duct acoustic calculations.
   Method is described in depth in 
   http://urn.fi/URN:NBN:fi:jyu-2011090611343

   Warning: Accuracy of the mesh is tolerable only for
   low frequencies (<500 Hz). 

   If cbc.blocks package is installed, it is used
   (quicker block-assembly).

   DOES NOT WORK WITH CURRENT VERSION OF SYMPY. PULL REQUESTS PENDING...
   Working version of SymPy: https://github.com/tuomas2/sympy/tree/all_fixes

   Also requires recent version of UFL and FFC ( >1.2.0 ).
   Can be obtained from there:
       https://bitbucket.org/fenics-project/ufl/branch/next
       https://bitbucket.org/fenics-project/ffc/branch/next

   2013 Tuomas Airaksinen
"""

########################################################################
# SEVERAL COEFFICIENTS & configuration
########################################################################

#If we have cbc.blocks installed, we want to use block form, because it is 
# a lot faster with real DOFS (A_i and B_i) that we are using in this example.
blocks_enabled = True
#blocks_enabled = False
rsep = False
#rsep = True
try:
    import block
except ImportError:
    blocks_enabled = False


nmodI = 3# Do not use too many modes, as of current version of dolfin (1.2.0)
nmodO = 3# assemble becomes very slow due to bad implementation of real DOFs.
         # see: 
         # http://fenicsproject.org/qa/1231/hybrid-fem-formulation-with-some-non-spatial-dofs-with-fenics
ax1 = 1
ax2 = 0

#Bessel degree for ith mode
nn = [ 0,  1,  2,  0,  3,  4,  1,  5,  2,  0,  6,  3,  1,  7,  4,  8,  2,
       0,  5,  9,  3,  1,  6, 10,  4, 11,  7,  2,  0, 12]

#f = 2500. #300./343.3 #Hz
c = 343.3 #m/s
#c = 1.#343.3 #m/s

#radii of ducts
r1 = r2 = 0.0336 #0.02385
#r1 = 0.5 #m
#r2 = 0.5 #m
vxI, vxO = 0.41, 0.275
rhov = 1.2
#rhov = 1.
from numpy import pi
f = 2950
kv = 2*pi*f/c
kcoefv = 1.0+0.0j #complex coefficient if damping is necessary

########################################################################
# SYMPY PART 
########################################################################
print "Sympy"
import sympy as sp
import numpy as np
import dolfin as df
from sympy import symbols
from complexdolfin.sympy_util import nabla_grad, dot, inner, grad, dx,ds
from sympy import *
cnj = conjugate

#define human readable names for boundaries
d_SoundHard = [ds(num) for num in range(3,20)]
d_Inflow = ds(1)
d_Outflow = ds(2)
d_Impedance = d_Inflow + d_Outflow 

#define some constant symbols
rho = symbols("rho",real=True) 
k = symbols("k",real=True)
kcoef = symbols("kcoef")

#define test and trial functions
p,v = symbols("p,v")
#weak form of Helmholtz Eq.
G = (
    inner(1/rho*grad(p),grad(v))
    -kcoef*k**2/rho*p*v
)*dx

#Trial and test functions for coefficients A_i and B_i
A  =  symbols("A:%d"%nmodI)
At =  symbols("At:%d"%nmodI)

B  =  symbols("B:%d"%nmodO)
Bt =  symbols("Bt:%d"%nmodO)

#Complex-valued coefficients for incoming sound boundary condition
fc  = symbols("fc:%d"%nmodI)

#Modal wave numbers
lambdaI = symbols("lambdaI:%d"%nmodI) 
lambdaO = symbols("lambdaO:%d"%nmodO) 
kappaI = symbols("kappaI:%d"%nmodI,real=True) 
kappaO = symbols("kappaO:%d"%nmodO,real=True) 

#Scaling coefficients
sclI = symbols("sclI:%d"%nmodI,real=True)
sclO = symbols("sclO:%d"%nmodO,real=True)

#Spatial coordinates, and radial coordinates, derived from spatial ones
xc = symbols("x:3",real=True)

x = (xc[0]-vxI,xc[1],xc[2])
rad = sqrt(x[ax1]**2+x[ax2]**2)
th = atan2(x[ax2],x[ax1])

#Weak form of matrix blocks that will correspond to coefficients A_i, i.e. outgoing sound
#at the inlet, and incoming soundsource term fvec

H = 0
Htilde = 0
Hhat = 0
ftildevec = 0
fvec = 0

for i in range(nmodI):
    PHI = sclI[i]*besselj(nn[i],kappaI[i]*rad) * exp(1.0j*nn[i]*th)

    Hhat      +=  1.j/rho * lambdaI[i] * A[i]  * PHI * v * d_Inflow # (26:1)
    ftildevec +=  1.j/rho * lambdaI[i] * fc[i] * PHI * v * d_Inflow # (26:4)
    H         +=  A[i] * PHI * cnj(PHI) * At[i] * d_Inflow # (29:1)
    Htilde    +=  - p * cnj(PHI) * At[i] * d_Inflow        #(29:2)
    fvec      +=  - PHI * fc[i] * cnj(PHI) * At[i] * d_Inflow # (29:5)

#Weak form of matrix blocks that will correspond to coefficients B_i, i.e. outgoing sound
#at the outlet

K = 0
Ktilde = 0
Khat = 0

x = (xc[0]-vxO,xc[1],xc[2])
rad = sqrt(x[ax1]**2+x[ax2]**2)
th = atan2(x[ax2],x[ax1])

for i in range(nmodO):
    PSI = sclO[i]*besselj(nn[i],kappaO[i]*rad)* exp(1.0j*nn[i]*th)

    Khat      +=  1.j/rho * lambdaO[i] * B[i] * PSI * v * d_Outflow #(26:2)
    K         +=  B[i] * PSI * cnj(PSI) * Bt[i] * d_Outflow #(29:3)
    Ktilde    +=  - p * cnj(PSI) * Bt[i] * d_Outflow #(29:4)

#because these variables are defined also in dolfin. We need them in building 
#sympy2ufl map
p_sym = p
k_sym = k
ds_sym = ds

########################################################################
#DOLFIN PART
########################################################################
from dolfin import *
from complexdolfin.dolfin_util import Constants, Constant, SymPy2UFL, ComplexFunctionSpace 
parameters["linear_algebra_backend"]="Epetra" #because of block assembly
parameters['form_compiler']['cpp_optimize'] = True

mesh = Mesh("geder3k.xml") 

#Get numeric values for these various vectors and coefficients
import hybrid_calc
lambdaIv,lambdaOv,kappaIv,kappaOv,sclIv,sclOv,fcv = hybrid_calc.calc(kv, r1, r2, nmodI, nmodO)

V_p = ComplexFunctionSpace(mesh, "CG", 1)
V_a = MixedFunctionSpace([ComplexFunctionSpace(mesh,"R",0) for i in range(nmodI) ])
V_b = MixedFunctionSpace([ComplexFunctionSpace(mesh,"R",0) for i in range(nmodO) ])

if not blocks_enabled:
    V_all = MixedFunctionSpace([V_p,V_a,V_b])

#Build a map between SymPy symbols and UFL variables/expressions etc.
S2U=SymPy2UFL(
{
    xc: [Expression("x[%d]"%i) for i in range(3)],
    k_sym: Constant(kv),
    kcoef: Constant(kcoefv),
    rho: Constant(rhov),
    lambdaI: Constants(lambdaIv[:nmodI]),
    lambdaO: Constants(lambdaOv[:nmodO]),
    kappaI: Constants(kappaIv[:nmodI]),
    kappaO: Constants(kappaOv[:nmodO]),
    sclI: Constants(sclIv[:nmodI]),
    sclO: Constants(sclOv[:nmodO]),
    fc: Constants(fcv[:nmodI]),
    ds_sym: ds[MeshFunction("size_t", mesh, 2, mesh.domains())],
    besselj: bessel_J,
    atan2 : atan_2, 
})

if blocks_enabled:
    S2U.add({
        p_sym: TrialFunctions(V_p),
        v: TestFunctions(V_p),
        A: TrialFunctions(V_a),
        At: TestFunctions(V_a),
        B: TrialFunctions(V_b),
        Bt: TestFunctions(V_b),})
else:
    S2U.add({
        (p_sym, A, B) : TrialFunctions(V_all), 
        (v, At, Bt) : TestFunctions(V_all)})

all_domains = (G + 
               H + Hhat  + Htilde + 
               K + Khat + Ktilde 
               -fvec-ftildevec)

#Convert SymPy expressions into UFL, by using mappings we have made
G = S2U(G) 
H = S2U(H) 
K = S2U(K)
Hhat = S2U(Hhat)
Htilde = S2U(Htilde)
Khat = S2U(Khat)
Ktilde = S2U(Ktilde)
fvecs = fvec
fvec = S2U(fvec)
ftildevec = S2U(ftildevec)

if not blocks_enabled:
    all_domains = S2U(all_domains)
    
    A = df.assemble(df.lhs(all_domains))
    b = df.assemble(df.rhs(all_domains))

    u = Function(V_all)
    df.solve(A,u.vector(),b)
    solutions = u.split()
    re_p,im_p = solutions[0]
    a_v = [ solutions[1][2*j]((0.,0.,0.))+solutions[1][2*j+1]((0.,0.,0.))*1.0j for j in range(nmodI)]
    b_v = [ solutions[2][2*j]((0.,0.,0.))+solutions[2][2*j+1]((0.,0.,0.))*1.0j for j in range(nmodO)]
else:
    from block import block_assemble, block_mat
    from block.iterative import LGMRES
    from block.algebraic.trilinos import AmesosSolver        
    print "Assemble 1"
    
    AA,AArhs=block_assemble([[G, Hhat, Khat],
                             [Htilde ,H, 0],
                             [Ktilde,0,  K]])

    [[G,     Hhat, Khat],
     [Htilde, H,   _ ],
     [Ktilde, _,   K ]] = AA
    
    bb = block_assemble([ftildevec,fvec,0])
    bb.allocate([V_p, V_a, V_b])
       
    print "Preconditioning" 
    
    #We precondition here with (almost) inverted matrix, i.e. iterative method
    #converges in a few iterations.

    Gp = AmesosSolver(G)
    Hp = AmesosSolver(H)
    Kp = AmesosSolver(K)

    AAp = block_mat([[Gp,0,0],
                     [0, Hp,0],
                     [0, 0, Kp]])
    print "Solve"
    
    Ainv = LGMRES(AA, precond=AAp, show=2, tolerance = 1e-12, 
                  maxiter=30, name="AA^")

    x = Ainv*bb
    u = Function(V_p)
    u.vector()[:] = x[0]
    re_p,im_p = u.split()

    #2n length real array to n-length complex array
    r_to_c = lambda v: np.array([v[2*i] + v[2*i+1]*1.j for i in range(len(v)/2)])
        
    a_v = r_to_c(x[1])
    print "a",a_v
    b_v = r_to_c(x[2])
    print "b",b_v

pf_file = df.File("re_p_file.pvd")
pf_file << df.project(re_p,df.FunctionSpace(mesh,"CG",1))

from hybrid_calc import cc, nrealO

y = 0.0
for i in range(nrealO):
    y += lambdaOv[i]*sclOv[i]**2*cc[i]*r2**2 * abs(b_v[i])**2

tlv = y / (cc[0]*sclIv[0]**2*r1**2)
tl = -10.0 * np.log10(tlv)

print "transmission loss",tl.real

df.plot(re_p,title = "re")
df.plot(im_p,title = "im")
df.plot(df.sqrt(re_p**2 + im_p**2), interactive=True, title="norm")
