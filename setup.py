#!/usr/bin/env python

from distutils.core import setup
#from setuptools import setup
#from distutils import sysconfig
from os.path import join as pjoin, split as psplit
#import sys
#import platform

# Version number
major = 0
minor = 1
#maintenance = '0+'
maintenance = 0
#maintenance = '-alpha'
#maintenance = '-beta'
#maintenance = '-rc'

if isinstance(maintenance, int): # Numbered release
    version = "%d.%d.%d" % (major, minor, maintenance)
else: # Pre-release (-alpha, -beta, -rc) or dev version (+, .0+)
    version = "%d.%d%s" % (major, minor, maintenance)

print
print version
print
requirements= [i.strip() for i in open("requirements.txt").readlines()]
from pip.req import parse_requirements
install_reqs = parse_requirements("requirements.txt")
reqs = [str(ir.req) for ir in install_reqs]

setup(name = "complexdolfin",
      version = version,
      description = "SymPy to UFL conversion tools to perform complex-valued PDE calculations with FeniCS/DOLFIN",
      author = "Tuomas Airaksinen",
      author_email = "tuomas.a.airaksinen@jyu.fi",
      url = "https://bitbucket.org/tuomas2/complex-dolfin",
      classifiers=[
          'Intended Audience :: Developers',
          'Intended Audience :: Science/Research',
          'Programming Language :: Python :: 2.6',
          'License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)',
          'Topic :: Scientific/Engineering :: Mathematics',
          'Topic :: Software Development :: Compilers',
          'Topic :: Software Development :: Libraries :: Python Modules',
          ],
      packages = ["complexdolfin"],
      package_dir = {"complexdolfin": "complexdolfin"},
      install_requires=reqs,
      )
