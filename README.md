COMPLEXDOLFIN
=============

SymPy to UFL conversion library to allow convenient
formulation of complex-valued PDEs with FeniCS/DOLFIN.

Requires SymPy >= 0.7.2 and FeniCS >= 1.2.

Project homepage: <https://bitbucket.org/tuomas2/complexdolfin>

Related projects:
----------------

FeniCS: <http://fenicsproject.org/>, <https://bitbucket.org/fenics-project>

SymPy:  <http://sympy.org/>, <http://github.com/sympy/sympy>

Installation
============

    python setup.py install

or 

    sudo python setup.py install

Usage
=====

Look at demos in demo/ folder.

Simple Helmholtz demo:

    :::python
    # SYMPY PART
    from sympy import *
    from complexdolfin.sympy_util import *

    # Define complex-valued and real-valued symbols
    u,v,f = symbols("u,v,f")
    rho,k = symbols("rho,k",real=True)

    # Helmholts equation with absorbing boundary condition, as a SymPy expression
    a = (1/rho*inner(grad(u),grad(v))*dx 
         - k**2*u*v*dx 
         - f*v*dx 
         - 1j*k*u*v*ds
         )

    k_sym = k 
    # DOLFIN PART
    from dolfin import *
    from complexdolfin.dolfin_util import * 

    mesh = UnitSquareMesh(30,30)
    V = ComplexFunctionSpace(mesh,"CG",1)

    # Define mapping between SymPy objects and DOLFIN objects
    S2U = SymPy2UFL({u :     TrialFunctions(V),
                     v :     TestFunctions(V),
                     k_sym : Constant(15.0),
                     rho :   Constant(1.2),
                     f :    Constant(0.0j)        })

    # Convert SymPy expression to UFL expression
    a = S2U(a)

    A = assemble(lhs(a))
    b = assemble(rhs(a))

    # Place source term in the middle
    try:
        from finddof import finddof
        centerdof = finddof((0.5,0.5),V)[0] 
    except: #finddof fails with dolfin <= 1.2.0 
        centerdof = int(len(b)/2+1) 

    b[centerdof] = 1.0 

    u = Function(V)
    solve(A,u.vector(),b)
    re_u, im_u = u.split()

    plot(re_u,interactive=True)

Licence
=======

(C) 2013 Tuomas Airaksinen

COMPLEXDOLFIN is free software: you can redistribute it and/or modify 
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

COMPLEXDOLFIN is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or 
FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public
License along with COMPLEXDOLFIN. If not, see <http://www.gnu.org/licenses/>.



