#encoding:utf-8
# Tuomas Airaksinen 2013
# Licenced under LGPL

from dolfin import *
import sympy
from sympy_util import d_syms,dp_syms
import ufl
from ufl.tensors import ListTensor
from ufl.indexed import Indexed
iterables = (tuple,list,ListTensor)

def isdoublelength(key,value):
    dl = False 
    if len(key) == len(value):
        pass
    elif 2*len(key) == len(value):
        dl = True
    else:
        raise Exception("Illegal length")
    return dl

def nestedtuples(k,v):
    """
    Generator that gives one (key,value) pair at once, and goes through
    nested structures
    k: (a,b,(c,d,(e,f,g)))
    v: (a,b,(c,d,(e,f,g)))
    should give one at time (a,a), (b,b), (c,c), (d,d), (e,e), (f,f) and (g,g).

    If the number of values is twise the number of keys, as:
    k: (a,b,(c,d))
    v: (a,b,(c,d,e,f))
    then it gives
    (c,(c,d)) and (d,(e,f)) (for the tuple presentation of complex numbers etc.)
    """
    if not isinstance(k,iterables):
        yield k,v
    else:
        dl = isdoublelength(k,v)
        for j in range(len(k)):
            k2 = k[j]
            if isinstance(k2,iterables):
                assert not dl
                v2 = v[j]
                for k3,v3 in nestedtuples(k2,v2):
                    yield k3,v3
            else:
                if dl:
                    v2 = (v[2*j],v[2*j+1])
                else:
                    v2 = v[j]
                yield k2,v2

class SymPy2UFL(object):
    """Convert SymPy expression to UFL
       
       Usage:
 
       s2u = SymPy2UFL(initial_map)
       s2u.add(additional_map)
       
       ufl_expression = s2u(sympy_expression)

       Here initial_map and additional_map are dicts that contains 
       map from SymPy object (or name) to UFL equivalent. These equivalents 
       can be also (nested) tuples or similar, if necessary. Some examples 
       in the following dict structure:

       s2umap = {
            (p, A, B): TrialFunctions(V), #tuple:tuple
            u: TrialFunctions(V),         #sym:tuple
            v: TestFunctions(V),          #sym:tuple
            x: [Expression("x[%d]"%i) for i in range(3)], #sym:tuple
            rho: Constant(rhov),          #sym:tuple
            realconstants: Constants([1.,2.,3.]), #tuple:tuple
            complexconstants: Constants([1.+2.j,2.+3.j,3.+4.j]), #tuple:tuple
            ds_sym: ds[mesh.domains().facet_domains()], #sym:UFL
            "varname": 2,               #str:obj
            besselj: bessel_J,          #sympy function: UFL
        }
    """
    replace = True
    d_map = {}

    def addtodict(self,d,key,value):
        if key in d and not self.replace:
            raise Exception("Key %s already in dict"%key)
        else:
            d[key]=value

    def __init__(self,dolfinmap={}):
        self.buildmap(dolfinmap)

    def buildmap(self,sympy_to_ufl_map,clear=True): 
        s = self
        if clear:
            self.d_map = {}
        
        d = self.d_map
        
        dfmap = sympy_to_ufl_map
        
        for key,value in nestedtuples(dfmap.keys(),dfmap.values()):
            if hasattr(key,"name"): #SymPy symbol 
                name = key.name
                if key in d_syms+dp_syms:
                    #ds,dx etc should not be renamed. 
                    s.addtodict(d,name,value)
                else:
                    if isinstance(value,iterables):
                        #complex-valued, two variables
                        assert len(value) == 2
                        s.addtodict(d,"__ss_re_%s"%(name),value[0])
                        s.addtodict(d,"__ss_im_%s"%(name),value[1])
                    else:
                        #real-valued, one variable
                        s.addtodict(d,"__ss_RE_%s"%name,value)

            elif isinstance(key,sympy.FunctionClass):
                #if key is sympy function
                name = key.__name__
                s.addtodict(d,name,value)
            elif isinstance(key,str):
                #if key is string
                s.addtodict(d,key,value)
            else:
                raise Exception("Unknown type",type(key))

    def add(self,sympy_to_ufl_map,replace=True):
        replace_old = self.replace
        self.replace = replace
        self.buildmap(sympy_to_ufl_map,clear=False)
        self.replace = replace_old

    def __setitem__(self,key,value):
        self.add({key:value})

    def __call__(self,expression):
        for key,value in self.d_map.iteritems():
            locals()[key] = value

        from sympy_util import SymPy2UFL_str
        a_str = SymPy2UFL_str(expression)
        try:
            return eval(a_str)
        except Exception as e:
            print("Error in evaluating string: %s"%a_str)
            raise e

