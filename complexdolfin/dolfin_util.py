#encoding:utf-8
# Tuomas Airaksinen 2013
# Licenced under LGPL

import dolfin as df
import numpy as np

def ComplexFunctionSpace(mesh,_type,degree):
    re_V = df.FunctionSpace(mesh, _type, degree)
    im_V = df.FunctionSpace(mesh, _type, degree)

    return df.MixedFunctionSpace([re_V,im_V])

def Constant(value):
    """
    Version of dolfin.Constant function that allows either
    real or complex-valued argument. 
    Returns tuple of Constant instances if complex argument.
    """
    if isinstance(value,complex):
        return (df.Constant(value.real), df.Constant(value.imag))
    else:
        return df.Constant(value)

def Constants(values):
    """
    For multiple constant definitions at once.
    Allows both real and complex numbers and acts accordingly.
    """
    l = []
    for idx,v in enumerate(values):
        l.append(Constant(v))
    return l

from sympytoufl import SymPy2UFL 
