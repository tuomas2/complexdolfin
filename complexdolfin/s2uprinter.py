# Tuomas Airaksinen 2013
# Licenced under LGPL

from sympy.core.function import _coeff_isneg
from sympy.printing.precedence import precedence 
from sympy.printing.python import PythonPrinter,StrPrinter
from sympy import * 

__all__ = ["S2UPrinter"]

class S2UPrinter(PythonPrinter):
    """
    Like PythonPrinter, but ensures that dx, ds etc. (or whatever there is in lastargs)
    are placed as last items in expression. Denominator is placed in front of the expression.
    Printer also converts re(var) to __ss_re_var and im(var) to __ss_im_var. Purely real variable var is 
    written as __ss_RE_var.
    """
    lastargs = [] 
    def _print_Float(self,expr):
        return str(expr)
    def _print_Function(self, expr):
        if expr.func == re:
            assert len(expr.args) == 1
            return "__ss_re_"+expr.args[0].name
        if expr.func == im:
            assert len(expr.args) == 1
            return "__ss_im_"+expr.args[0].name

        return PythonPrinter._print_Function(self, expr)

    def _print_Symbol(self, expr):
        import dolfin
        symbol = self._str(expr)
        if symbol not in self.symbols:
            self.symbols.append(symbol)
        if (expr in self.lastargs or 
            (len(expr.args) > 0 and hasattr(dolfin,symbol))):
            return StrPrinter._print_Symbol(self,expr)
        else:
            return "__ss_RE_"+expr.name

    def _print_Rational(self,expr):
        return StrPrinter._print_Rational(self,expr)
            
    def _print_Mul(self, expr):

        prec = precedence(expr)

        if _coeff_isneg(expr):
            expr = -expr
            sign = "-"
        else:
            sign = ""

        a = [] # items in the numerator
        b = [] # items that are in the denominator (if any)

        if self.order not in ('old', 'none'):
            args = expr.as_ordered_factors()
        else:
            # use make_args in case expr was something like -x -> x
            args = Mul.make_args(expr)
        
        args = list(args)
        args_classes = [i.__class__ for i in args]

        for la in self.lastargs:
            if la in args_classes:
                la = args[args_classes.index(la)] #to ensure that ds(*) etc is also moved to the end
            if la in args: 
                args.remove(la)
                args.append(la)
        args = tuple(args)

        # Gather args for numerator/denominator
        for item in args:
            if item.is_commutative and item.is_Pow and item.exp.is_Rational and item.exp.is_negative:
                if item.exp != -1:
                    b.append(Pow(item.base, -item.exp, evaluate=False))
                else:
                    b.append(Pow(item.base, -item.exp))
            elif item.is_Rational and item is not S.Infinity:
                if item.p != 1:
                    a.append(Rational(item.p))
                if item.q != 1:
                    b.append(Rational(item.q))
            else:
                a.append(item)

        a = a or [S.One]

        a_str = map(lambda x:self.parenthesize(x, prec), a)
        b_str = map(lambda x:self.parenthesize(x, prec), b)
        if len(b) == 0:
            return sign + '*'.join(a_str)
        else:
            return sign + "(1./(%s))*"%'*'.join(b_str) + '*'.join(a_str) 
